# Gallery #

This app shows how to make web content available to users by including a WebView in the layout. The app also shows how to handle runtime permission, also known as dangerous permissions, by asking for permission to to use the camera. In AndroidStudio a new project is created with an empty activity.

## The Gallery Web Page ##

At the web address [https://dt031g.programvaruteknik.nu/gallery/](https://dt031g.programvaruteknik.nu/gallery/) there is an image gallery that displays images. Users can upload images to the gallery by clicking the upload button and then select an image that is stored on their device (computer/mobile/tablet).

![The start page of the Gallery web app shown on a desktop computer](screenshots/gallery-desktop-index.png "The start page of the Gallery web app shown on a desktop computer")

![The add page of the Gallery web app shown on a desktop computer](screenshots/gallery-desktop-add.png "The add page of the Gallery web app shown on a desktop computer")

## The Gallery Android App ##

The purpose of the app is to:

1. Make use of the existing Gallery web page, by displaying it in a `WebView` for the user.
2. Modify the 'add image' functionality by letting the user take pictures with the device's camera, which is then uploaded to the gallery.

### Adding the WebView ###

Following the instructions [here](https://developer.android.com/guide/webapps/webview#AddingWebView), we start by adding a `WebView` to the `activity_main` layout and let it occupy the whole screen. Be sure to give the view an id so that we later can get a reference to in the Java code:

```xml
<WebView
    android:id="@+id/web_view"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    app:layout_constraintBottom_toBottomOf="parent"
    app:layout_constraintLeft_toLeftOf="parent"
    app:layout_constraintRight_toRightOf="parent"
    app:layout_constraintTop_toTopOf="parent" />
```

#### Loading a web page ####

In order for the web page to be load in the `WebView`, the app needs the [`INTERNET`](https://developer.android.com/reference/android/Manifest.permission#INTERNET) permission added to the app's manifest file (as a direct child to the `manifest` element):

```xml
<uses-permission android:name="android.permission.INTERNET" />
```

In `MainActivity.onCreate` we get a reference to the `WebView` and call the `loadUrl` method providing the URL to the gallery as an argument:

```java
private static final String URL_GALLERY = "https://dt031g.programvaruteknik.nu/gallery/";
...
WebView webView = findViewById(R.id.web_view);
webView.loadUrl(URL_GALLERY);
```

If we run the app, the web page should now be displayed in the app as shown below:

![The start page of the Gallery Android app shown in an Android app (in portrait mode)](screenshots/gallery-android-index-portrait.png "The start page of the Gallery Android app shown in an Android app (in portrait mode)")

![The start page of the Gallery Android app shown in an Android app (in landscape mode)](screenshots/gallery-android-index-landscape.png "The start page of the Gallery Android app shown in an Android app (in landscape mode)")

#### Intercept URL loading ####

If the user clicks on the add image button the page, the page is not opened in our app, but in the device's default app for handling URL's (normally the Chrome app).

![The add image page of the Gallery Android app shown in Chrome app](screenshots/gallery-android-add-default-portrait.png "The add image page of the Gallery Android app shown in Chrome app")

This is not the behavior we want. When the user clicks on this button we want the camera to start. To intercept URL loading in a `WebView`, we create a subclass of [`WebViewClient`](https://developer.android.com/reference/android/webkit/WebViewClient) and pass an instance of this class as an argument to the `WebView.setWebViewClient` method. As this is the only link on the page, we do not need to check which URL is being opened. We do it anyway in this example just to show how it can be done:

```java
private static final String URL_GALLERY_ADD = "https://dt031g.programvaruteknik.nu/gallery/add.php";
...
webView.setWebViewClient(new WebViewClient() {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (url.equalsIgnoreCase(URL_GALLERY_ADD)) {
            startActivity(new Intent(getApplicationContext(), CameraActivity.class));
            return true; // stops current WebView from loading the URL
        }

        return false; // let current WebView continue loading the URL
    }
});
```

Instead of the add image page being loaded in the `WebView`, we now intercept the URL loading and start our `CameraActivity` for taking a photo with the device's camera and to uploading the photo to the Gallery web app. By returning `true` we tell the `WebView` that we will handle the "loading" ourselves. For all other URL's we return `false`, which means the `WebView` should continue with loading the content of the URL.

### Using the camera ###

Using the camera in an Android app is not a part of this course. Therefor you do not have to understand all the code related to the camera usage. In this example we focus on the parts that show how to handle runtime-permissions (dangerous permissions) and to perform long-running tasks in a background thread. The [Getting Started with CameraX](https://codelabs.developers.google.com/codelabs/camerax-getting-started) Codelab from Google is followed in this example. The only difference is where they use `MainActivity`, we use `CameraActivity`.

CameraX is a Jetpack support library, built to help us make camera app development easier. In order for us to use this library, we need to add the CameraX dependencies to our app's Gradle file:

```
// CameraX core library using camera2 implementation
implementation 'androidx.camera:camera-camera2:1.1.0-alpha04'
// CameraX Lifecycle Library
implementation 'androidx.camera:camera-lifecycle:1.1.0-alpha04'
// CameraX View class
implementation 'androidx.camera:camera-view:1.0.0-alpha24'
```
    
### CameraActivity ###

We create a new empty activity, named `CameraActivity`. Using the camera in Android is a so called dangerous permission and the user needs to grant the app permission to use the camera. We add the [`CAMERA`](https://developer.android.com/reference/android/Manifest.permission#CAMERA) permission to the app's manifest file:

```xml
<uses-permission android:name="android.permission.CAMERA" />
```

Since the camera uses specific hardware on the device, we should add an `uses-feature` element in the app manifest. By specifying the features that an application requires, we enable Google Play to present an application only to users whose devices meet the application's feature requirements, rather than presenting it to all users. In most cases, hardware is optional, so it's better to declare the hardware as optional by setting `android:required` to false in the `uses-feature` declaration. This is the case in our app. We do not need the camera for the app to work. The user can still look at already added photos in the Gallery with out using a camera.

```xml
<uses-feature android:name="android.hardware.camera.any"
    android:required="false"/>
```

Adding [`android.hardware.camera.any`](https://developer.android.com/guide/topics/manifest/uses-feature-element#camera-hw-features) makes sure that the app only can be installed on device's that have a camera (when installed from Google Play). Specifying .any means that it can be a front camera or a back camera.

#### Implement camera preview (step 1/2) ####

CameraX introduces use cases, which allow you to focus on the task you need to get done instead of spending time managing device-specific nuances. There are several basic use cases including [Preview](https://developer.android.com/training/camerax/preview) which helps us get an image stream from the camera displayed in our app's UI. The [`PreviewView`](https://developer.android.com/reference/androidx/camera/view/PreviewView) is a Custom View that displays the camera feed for CameraX's Preview use case.

In the `activity_camera` layout file, we add a `PreviewView` together with an `ImageButton` to act as the capture button:

```xml
<androidx.constraintlayout.widget.ConstraintLayout
    ...>

    <androidx.appcompat.widget.AppCompatImageButton
        android:id="@+id/camera_capture_button"
        android:background="@drawable/ic_camera_capture"
        ... />
        
    <androidx.camera.view.PreviewView
        android:id="@+id/viewFinder"
        android:layout_width="match_parent"
        android:layout_height="match_parent" />
        
</androidx.constraintlayout.widget.ConstraintLayout>
```
 
When running the code, the camera activity now looks like this:

![First test of the CameraActivity](screenshots/gallery-android-camera-1.png "First test of the CameraActivity")
 
#### Request camera permissions ####

Using the camera in Android is listed as a dangerous permissions, also called runtime permission, that the user has to approve before an app can use the camera. It is the app that requests (asks for) one or more permissions during runtime. The principal is to ask for permissions in context, when the user starts to interact with the feature that requires it. It is then up to the user to decide if they want to grant or deny the app this feature (camera in our case). We can not assume that the user always grant all permissions an app asks for.
 
Before we declare and request runtime permissions in an app, we should always evaluate whether the app really needs this permission. Often an app can fulfill many use cases, such as taking photos, without needing to declare any permissions. In this app we really should use an Intent that starts the device's own camera app to take the photo. But to give an example how to request runtime permissions we handle the camera ourselves.

The workflow for requesting permissions are as follow:

1. In your app's manifest file, declare the permissions that your app might need to request.

    - Done. We have added the `CAMERA` permission in our manifest file.
   
2. Design your app's UX so that specific actions in your app are associated with specific runtime permissions. Users should know which actions might require them to grant permission for your app to access private user data.
   
    - Done. We have an add image button on the web page that starts our CameraActivity which needs the permission to use the camera.

3. Wait for the user to invoke the task or action in your app that requires access to specific private user data. At that time, your app can request the runtime permission that's required for accessing that data.

    - Not done. We will request the permission once the `CameraActivity.onCreate` is invoked.

4. Check whether the user has already granted the runtime permission that your app requires. If so, your app can access the required feature. If not, continue to the next step. NOTE! You must check whether you have that permission every time you perform an operation that requires that permission.

    - Not done. We will do this check in the `CameraActivity.onCreate` method.

5. Check whether your app should show a rationale to the user, explaining why your app needs the user to grant a particular runtime permission. If the system determines that your app shouldn't show a rationale, continue to the next step directly, without showing a UI element.

    If the system determines that your app should show a rationale, however, present the rationale to the user in an UI element. This rationale should clearly explain what data your app is trying to access, and what benefits the app can provide to the user if they grant the runtime permission. After the user acknowledges the rationale, continue to the next step.

    - Not done. We will implement this in the `CameraActivity.onCreate` method.

6. Request the runtime permission that your app requires in order to access the private user data. The system displays a runtime permission prompt.

    - Not done. The permission will be requested in the `CameraActivity.onCreate` method.

7. Check the user's response, whether they chose to grant or deny the runtime permission.

    - Not done. The check will be done in the `CameraActivity.onCreate` method.

8. If the user granted the permission to your app, you can access the private user data or feature. If the user denied the permission instead, gracefully degrade your app experience so that it provides functionality to the user, even without the information that's protected by that permission.

    - Not done. We implement this in `CameraActivity`. If the permission to use the camera is granted we start the camera. If the permission is denied we show a `Toast` message and finish the `CameraActivity`.

##### Check if a permission is already granted #####

To check if the user has already granted your app a particular permission, pass that permission into the [`ContextCompat.checkSelfPermission`](https://developer.android.com/reference/androidx/core/content/ContextCompat#checkSelfPermission(android.content.Context,%20java.lang.String)) method. This method returns either [`PERMISSION_GRANTED`](https://developer.android.com/reference/android/content/pm/PackageManager#PERMISSION_GRANTED) or [`PERMISSION_DENIED`](https://developer.android.com/reference/android/content/pm/PackageManager#PERMISSION_DENIED), depending on whether your app has the permission.

Last in `CameraActivity.onCreate` we add the following code:

```java
// Check/request camera permission
if (cameraPermissionGranted()) {
    startCamera();
} else {
    requestCameraPermission();
}
```

Here we start by calling a method that will check if the `CAMERA` permission is already granted. If granted, we call a method that will start the camera. If not granted, we call a method that starts the process of requesting the `CAMERA` permission.

Add the `cameraPermissionGranted` method in `CameraActivity`:

```java
private boolean cameraPermissionGranted() {
    return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
            PackageManager.PERMISSION_GRANTED;
}
```

This method will return true if the permission to use the camera is already granted to the app.

##### Request a runtime permission #####

When requesting for permissions, the system displays an UI for the user asking the user to grant or deny the request. A request is made by invoking [`ActivityCompat.requestPermissions`](https://developer.android.com/reference/androidx/core/app/ActivityCompat#requestPermissions(android.app.Activity,%20java.lang.String%5B%5D,%20int)), were one of the arguments to this method is a request code. After the user has granted or denied the requested permissions you will receive a callback reporting whether the permissions were granted or not. In this callback we will get the request code used when requesting the permission.

Traditionally, you manage a request code yourself as part of the permission request and include this request code in your permission callback logic. Another option is to use the [`RequestPermission` contract](https://developer.android.com/reference/androidx/activity/result/contract/ActivityResultContracts.RequestPermission), included in an AndroidX library, where you allow the system to manage the permission request code for you. Because using the `RequestPermission` contract simplifies your logic, it's recommended that you use it when possible. Just like the CameraX Jetpack support library simplifies camera app development, the [`Activity` Jetpack support library](https://developer.android.com/jetpack/androidx/releases/activity) simplifies handling of request codes (among other things).

To allow the system to manage the request code that's associated with a permissions request, add dependencies on the following libraries in your module's build.gradle file (the Fragment library is only needed if you use Fragments):

```text
// AndroidX Activity Jetpack library
implementation 'androidx.activity:activity:1.2.2'
// AndroidX Fragment Jetpack library
implementation 'androidx.fragment:fragment:1.3.3'
```

You can then use one of the following classes:

- To request a single permission, use [`RequestPermission`](https://developer.android.com/reference/androidx/activity/result/contract/ActivityResultContracts.RequestPermission)
- To request multiple permissions at the same time, use [`RequestMultiplePermissions`](https://developer.android.com/reference/androidx/activity/result/contract/ActivityResultContracts.RequestMultiplePermissions)

The following steps show how to use the `RequestPermission contract`:

1. In your activity or fragment's initialization logic, pass in an implementation of [`ActivityResultCallback`](https://developer.android.com/reference/androidx/activity/result/ActivityResultCallback) into a call to [`registerForActivityResult`](https://developer.android.com/reference/androidx/activity/result/ActivityResultCaller#registerForActivityResult(androidx.activity.result.contract.ActivityResultContract%3CI,%20O%3E,%20androidx.activity.result.ActivityResultCallback%3CO%3E)). The `ActivityResultCallback` defines how your app handles the user's response to the permission request.

    Keep a reference to the return value of `registerForActivityResult`, which is of type [`ActivityResultLauncher`](https://developer.android.com/reference/androidx/activity/result/ActivityResultLauncher).

    - Not done. We will do ths by declaring a private field `requestPermissionLauncher` of the type `ActivityResultLauncher<String>` in `CameraActivity`.

2. To display the system permissions dialog when necessary, call the [`launch`](https://developer.android.com/reference/androidx/activity/result/ActivityResultLauncher#launch(I)) method on the instance of `ActivityResultLauncher` that you saved in the previous step (`requestPermissionLauncher`).

    After `launch` is called, the system permissions dialog appears. When the user makes a choice, the system asynchronously invokes your implementation of `ActivityResultCallback`, which you defined in the previous step.

    - Not done. Will we call `launch` on our `requestPermissionLauncher` in the `requestCameraPermission` method in the `CameraActivity`.

We implement this method like this:

```java
private void requestCameraPermission() {
    // The registered ActivityResultCallback gets the result of this request
    requestPermissionLauncher.launch(Manifest.permission.CAMERA);
    }
```

which will show a system permissions dialog like shown below:

![A system dialog that ask for permission to use the camera (take pictures and videos)](screenshots/gallery-android-camera-permission-dialog.png "A system dialog that ask for permission to use the camera (take pictures and videos)")

The `requestPermissionLauncher` is declared like this:

```java
// Register the permissions callback, which handles the user's response to the
// system permissions dialog. Save the return value, an instance of
// ActivityResultLauncher, as an instance variable.
private ActivityResultLauncher<String> requestPermissionLauncher =
    registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
        if (isGranted) {
            // Permission is granted. Continue the action or workflow in your app.
            startCamera();
        } else {
            // Explain to the user that the feature is unavailable because the
            // features requires a permission that the user has denied. At the
            // same time, respect the user's decision. Don't link to system
            // settings in an effort to convince the user to change their decision.
            showMessageAndFinish(getString(R.string.camera_permission_needed));
        }
    });
```

In the callback, the 2nd argument in the `registerForActivityResult` method call, we get a `boolean` (`isGranted`) which is `true` if the user granted the permission. Then we now it is safe to call our `startCamera` method. If the user did not grant us the permission to use the camera (`isGranted` is `false`) there is no use in letting the CameraActivity continue its execution. We explain why we need the permission and finish the activity by calling our `showMessageAndFinish` method:

```xml
<string name="camera_permission_needed">To add an image the app needs permission to use the camera!</string>
```

```java
// Show a Toast message to the user and then exit the activity
private void showMessageAndFinish(String message) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    finish();
}
```

This will take the user back to `MainActivity` while displaying a `Toast` message to the user:

![A Toast message explaining why we need the permission to use the camera](screenshots/gallery-android-camera-permission-denied.png "A Toast message explaining why we need the permission to use the camera")

##### Show a rationale to the user #####

If the user denies a permission an app needs, but keep trying to use the feature in the app (that requires the permission being denied), we should explain to the user why the requested permission is needed. To help us decide if a rationale (explanation) should be display we call the [`shouldShowRequestPermissionRationale`](https://developer.android.com/reference/androidx/core/app/ActivityCompat#shouldShowRequestPermissionRationale(android.app.Activity,%20java.lang.String)) method. This method returns `true` or `false` depending on if a rational should be displayed or not for a given permission. This check should be done before requesting a permission.

In our `requestCameraPermission` method we make the following additions:

```java
private void requestCameraPermission() {
    // Check if a 'permission rationale UI' should be displayed (for the CAMERA permission)
    if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
        displayCameraPermissionRational(); // if so, show an AlertDialog in a DialogFragment
    }
    else {
        // The registered ActivityResultCallback gets the result of this request
        requestPermissionLauncher.launch(Manifest.permission.CAMERA);
    }
}
```

Following the steps described in [Displaying dialogs with DialogFragment](https://developer.android.com/guide/fragments/dialogs) we create a `DialogFragment` named `CameraPermissionNeededDialogFragment` that shows an `AlertDialog` which informs the user about the needed permission. To this dialog fragment we add an constructor which takes an `ActivityResultLauncher<String>` as argument. In the `displayCameraPermissionRational` we create an `CameraPermissionDialogFragment` passing our `requestPermissionLauncher` so that we can use that to 'launch the request for permission' when the user clicks on the OK button in the dialog. If the user clicks on the Cancel button we finish to activity with a call `requireActivity.finish()`.

Whenever the call to `shouldShowRequestPermissionRationale` returns `true`, the following dialog is shown for the user:

!["A dialog is shown to the user explaining why we need the permission to use the camera"](screenshots/gallery-android-camera-permission-rational.png "A dialog is shown to the user explaining why we need the permission to use the camera")

#### Implement camera preview (step 2/2) ####

Now when the handling of the CAMERA runtime permission is implemented, we can continue with finalizing the displaying camera preview by adding the below code to our `startCamera` method. I will not explain this code and you do not need to understand it.

```java
 private void startCamera() {
    ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

    cameraProviderFuture.addListener(() -> {
        // Used to bind the lifecycle of cameras to the lifecycle owner
        ProcessCameraProvider cameraProvider = null;
        try {
            cameraProvider = cameraProviderFuture.get();
        } catch (ExecutionException | InterruptedException e) {
            return; // TODO: show user an error message!
        }

        // The PreviewView in the activity's layout file
        PreviewView viewFinder = findViewById(R.id.viewFinder);

        // Preview
        Preview preview = new Preview.Builder().build();
        preview.setSurfaceProvider(viewFinder.getSurfaceProvider());

        // Select back camera as a default
        CameraSelector cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;

        try {
            // Unbind use cases before rebinding
            cameraProvider.unbindAll();

            // Bind use cases to camera
            cameraProvider.bindToLifecycle(
                    this, cameraSelector, preview);

        } catch(Exception e) {
            Log.e(TAG, "Use case binding failed: ", e);
        }

    }, ContextCompat.getMainExecutor(this));
}
```

If the user has granted the permission to use the camera, the camera preview should now be displayed in the app.
 
!["Camera preview shown in CameraActivity"](screenshots/gallery-android-camera-preview.jpg "Camera preview shown in CameraActivity")
 
#### Implement camera image capture ####
 
Now that we have the preview of the camera it is time to implement the actual image capture. This is done using the [`Image Capture`](https://developer.android.com/training/camerax/take-photo) use case. The image capture use case is designed for capturing high-resolution, high-quality photos and provides auto-white-balance, auto-exposure, and auto-focus (3A) functionality. There are two options how to store the captured image:

    - As an in-memory buffer
    - Saved to a provided file location on the device

Since we are going to upload the image to a web server we choose the first option, to have the captured image in a buffer. `ImageCapture` is the use case class for taking a picture, just like the `Preview` is for the use case for previewing picture from the camera.

First we declare an instance variable of type `ImageCapture` in `CameraActivity`:

```java
// A use case for taking a picture
private ImageCapture imageCapture;
```

In `onCreate` we then set a onclick listener on the 'take photo' button that calls a new `takePhoto` method:

```java
// Call takePhoto when the capture button gets clicked
findViewById(R.id.camera_capture_button).setOnClickListener(view -> takePhoto());
```

In the `startCamera` method we create the ImageCapture use case:

```java
// Build the use case for taking camera pictures
imageCapture = new ImageCapture.Builder().build();
```

We bind this use case to the life cycle owner (same owner as for the Preview use case). The state of the lifecycle will determine when the cameras are open, started, stopped and closed. When started, the use cases receive camera data.

```java
// Bind use cases (preview and image capture) to camera
cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture);
```

Just like with the `startCamera` method, I will not explain the code in the `takePhoto` method and you do not need to understand it. What is important is the callback method `onCaptureSuccess`, which is called if the image capture is successful. In this method we start the execution of an `AsyncTask` to do the actual upload of the image to the Gallery web app.

```java
// Takes a photo and "stores it" in a in-memory buffer
private void takePhoto() {
    // If the image capture use case is null, exit out of the method.
    // This will be null If you tap the photo button before image capture is set up.
    // Without the return statement, the app would crash if it was null.
    if (imageCapture == null) {
        return;
    }

    // Set up image capture listener, which is triggered after photo has been taken
    imageCapture.takePicture(
        ContextCompat.getMainExecutor(this), new ImageCapture.OnImageCapturedCallback() {
            @Override
            public void onCaptureSuccess(@NonNull ImageProxy image) {
                // Do the upload in an AsyncTask, providing the image to upload
                imageUploadAsyncTask = new ImageUploadAsyncTask();
                imageUploadAsyncTask.execute(image);
            }

            @Override
            public void onError(@NonNull ImageCaptureException e) {
                // TODO: Show an error message to the user
                Log.e(TAG, "Photo capture failed: ", e);
                finish(); // finish the activity
            }
        }
     );
}
```
#### AsyncTask ####

[`AsyncTask`](https://developer.android.com/reference/android/os/AsyncTask) is designed to be a helper class for performing an asynchronous task. An asynchronous task is defined by a computation that runs on a background thread and whose result is published on the UI thread. `AsyncTask`s should ideally be used for short operations (a few seconds at the most.) If you need to keep threads running for long periods of time, it is highly recommended you use the various APIs provided by the `java.util.concurrent` package.

An asynchronous task is defined by 3 generic types, called `Params`, `Progress` and `Result`, and 4 steps, called `onPreExecute`, `doInBackground`, `onProgressUpdate` and `onPostExecute`.

The three types used by an asynchronous task are the following:

    - `Params`, the type of the parameters sent to the task upon execution.
    - `Progress`, the type of the progress units published during the background computation.
    - `Result`, the type of the result of the background computation.

Not all types are always used by an asynchronous task. To mark a type as unused, simply use the type `Void`.

When an asynchronous task is executed, the task goes through 4 steps:

    1. `onPreExecute()`, invoked on the UI thread before the task is executed. This step is normally used to setup the task, for instance by showing a progress bar in the user interface.
    2. `doInBackground(Params...)`, invoked on the background thread immediately after `onPreExecute` finishes executing. This step is used to perform background computation that can take a long time. The parameters of the asynchronous task are passed to this step. The result of the computation must be returned by this step and will be passed back to the last step. This step can also use `publishProgress(Progress...)` to publish one or more units of progress. These values are published on the UI thread, in the `onProgressUpdate(Progress...)` step.
    3. `onProgressUpdate(Progress...)`, invoked on the UI thread after a call to `publishProgress(Progress...)`. The timing of the execution is undefined. This method is used to display any form of progress in the user interface while the background computation is still executing. For instance, it can be used to animate a progress bar or show logs in a text field.
    4. `onPostExecute(Result)`, invoked on the UI thread after the background computation finishes. The result of the background computation is passed to this step as a parameter.

#### ImageUploadAsyncTask ####
AsyncTask must be subclassed to be used. For our use case we create class named `ImageUploadAsyncTask` with these three type parameters:

```java
private class ImageUploadAsyncTask extends AsyncTask<ImageProxy, Integer, String>
```

The first type parameter, `ImageProxy`, is the in-memory image buffer we got from the `ImageCapture` use case. The `ImageProxy` is sent to the `doInBackground` method via the `execute` method call on the `ImageUploadAsyncTask` object we create:

```java
// In CameraActivity as an instance variable
// An asynchronous task for uploading a image to the Gallery web app
private ImageUploadAsyncTask imageUploadAsyncTask;
...
// In takePhoto -> onCaptureSuccess callback
// Do the upload in an AsyncTask, provide the image to upload
imageUploadAsyncTask = new ImageUploadAsyncTask();
imageUploadAsyncTask.execute(image);
...
// In ImageUploadAsyncTask
@Override
protected String doInBackground(ImageProxy... imageProxies) {
    // For easy access to the image
    ImageProxy image = imageProxies[imageProxies.length - 1];
...
```

The second type parameter, `Integer`, is for updating the upload progress (total bytes uploaded so far) during the upload in `doInBackground`. The `Integer` is sent to the `onProgressUpdate` method via the `publishProgress` method call:

```java
// In doInBackground (out=OutputStream connected to web server, imageStream=ByteBufferReader connected to the image buffer)
// Continue as long we are not cancelled and there are more data to upload
while (!isCancelled() && ((bytesRead = imageStream.read(buffer)) > 0)) {
    out.write(buffer, 0, bytesRead);
    totalBytesRead += bytesRead;
    publishProgress(totalBytesRead); // onProgressUpdate
}
...
@Override
protected void onProgressUpdate(Integer... values) {
    // Only publish the latest value
    progressBar.setProgress(values[values.length - 1]);
...
```

In `onProgressUpdate`, which is called on the main thread (UI thread), we update a `ProgressBar` with the total bytes written so far (gow the progress bar is created will be explained soon).

The third type parameter, `String`, is the type returned from `doInBackground`. In our case we return a `String` with a success/fail message (the response from the server after upload is done or has failed). This `String` is passed to `onPostExecute`.

```java
// In doInBackground
Scanner in = new Scanner(connection.getInputStream());
response = in.nextLine();
return response; // to onPostExecute
...
@Override
protected void onPostExecute(String result) {
    // Hide the progress layout
    progressBarViewGroup.setVisibility(View.GONE);

    // Check response/result from upload
    if (result.equalsIgnoreCase("ok")) {
        Toast.makeText(getApplicationContext(), R.string.add_image_ok, Toast.LENGTH_SHORT).show();
    }
    else {
        Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
    }

    finish(); // return to gallery (MainActivity)
}
```

To show the progress of the upload to the user we use a custom progress bar layout. This layout is created in `activity_camera.xml` on top of the camera preview.

```xml
<androidx.constraintlayout.widget.ConstraintLayout
    android:id="@+id/camera_upload"
    android:layout_width="match_parent"
    android:layout_height="100dp"
    android:background="?android:attr/colorBackground"
    android:elevation="2dp"
    android:padding="10dp"
    android:visibility="gone"
    app:layout_constraintBottom_toBottomOf="parent"
    app:layout_constraintLeft_toLeftOf="parent"
    app:layout_constraintRight_toRightOf="parent">

    <TextView
        android:id="@+id/camera_upload_title"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/upload"
        android:textSize="18sp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <ProgressBar
        android:id="@+id/camera_upload_progress"
        style="?android:attr/progressBarStyleHorizontal"
        android:layout_width="0dp"
        android:layout_height="wrap_content"
        android:layout_marginStart="10dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toEndOf="@+id/camera_upload_title"
        app:layout_constraintTop_toTopOf="parent" />
</androidx.constraintlayout.widget.ConstraintLayout>
```

Initial, this view is set to be `GONE`. When the upload starts, the view is set to be `VISIBLE`. After the upload is done, the view once again is set to be `GONE`. It is in the onPreExecute method, which is invoked on the UI thread before the task is executed, we set up the progress bar:

```java
@Override
protected void onPreExecute() {
    // Get references to the progressbar and its parent
    progressBar = findViewById(R.id.camera_upload_progress);
    progressBarViewGroup = findViewById(R.id.camera_upload);

    // Show the progress bar layout
    progressBarViewGroup.setVisibility(View.VISIBLE);
}
...
// In doInBackground, when we have a byte buffer from the image
progressBar.setMax(parameters.toString().length());
```

While the upload is ongoing, the progress is shown to the user like this:

!["Upload progress shown in a custom progress layout"](screenshots/gallery-android-camera-upload.png "Upload progress shown in a custom progress layout")

When returned to the main activity, the added image is in the gallery:

!["Added image is shown in the Gallery"](screenshots/gallery-android-index-portrait-image-added.png "Added image is shown in the Gallery")

# Final words #

Using the camera to show how runtime permissions are handled and how long-running tasks are done in an `AsyncTask` are done correctly, turned out to be more cumbersome than I had initially imagined. Focus not so much on the code for handling the camera, image capture and image upload, but more on the flow for handling permissions and using an `AsyncTask`.