package se.miun.roberi.dt031g.gallery;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.camera.view.PreviewView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.transition.Slide;
import androidx.transition.Transition;
import androidx.transition.TransitionManager;
import com.google.common.util.concurrent.ListenableFuture;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import javax.net.ssl.HttpsURLConnection;

public class CameraActivity extends AppCompatActivity {
    private static final String TAG = "CameraActivity";

    // Register the permissions callback, which handles the user's response to the
    // system permissions dialog. Save the return value, an instance of
    // ActivityResultLauncher, as an instance variable.
    private final ActivityResultLauncher<String> requestPermissionLauncher =
        registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
            if (isGranted) {
                // Permission is granted. Continue the action or workflow in your app.
                startCamera();
            } else {
                // Explain to the user that the feature is unavailable because the
                // features requires a permission that the user has denied. At the
                // same time, respect the user's decision. Don't link to system
                // settings in an effort to convince the user to change their decision.
                showMessageAndFinish(getString(R.string.camera_permission_needed));
            }
        });

    // A use case for taking a picture
    private ImageCapture imageCapture;

    // An asynchronous task for uploading a image to the Gallery web app
    private ImageUploadAsyncTask imageUploadAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        // Check/request camera permission
        if (cameraPermissionGranted()) {
            startCamera();
        } else {
            requestCameraPermission();
        }

        // Call takePhoto when the capture button gets clicked
        findViewById(R.id.camera_capture_button).setOnClickListener(view -> takePhoto());
    }


    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

        // Cancel any ongoing upload
        if (imageUploadAsyncTask != null) {
            Log.d(TAG, "cancelling task");
            imageUploadAsyncTask.cancel(true);
        }
    }

    // Returns true if permission to use CAMERA is already granted
    private boolean cameraPermissionGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED;
    }

    // Request permission to use the camera
    private void requestCameraPermission() {
        // Check if a 'permission rationale UI' should be displayed (for the CAMERA permission)
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
            displayCameraPermissionRational(); // if so, show an AlertDialog in a DialogFragment
        }
        else {
            // The registered ActivityResultCallback gets the result of this request
            requestPermissionLauncher.launch(Manifest.permission.CAMERA);
        }
    }

    // Displays a rational for the user explaining why the camera is needed
    private void displayCameraPermissionRational() {
        new CameraPermissionNeededDialogFragment(requestPermissionLauncher).show(
                getSupportFragmentManager(), CameraPermissionNeededDialogFragment.TAG
        );
    }

    // Show a Toast message to the user and then exit the activity
    private void showMessageAndFinish(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        finish();
    }

    // Starts the camera (displays camera stream in our preview view)
    private void startCamera() {
        ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(() -> {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            ProcessCameraProvider cameraProvider;
            try {
                cameraProvider = cameraProviderFuture.get();
            } catch (ExecutionException | InterruptedException e) {
                return; // TODO: show user an error message!
            }

            // The PreviewView in the activity's layout file
            PreviewView viewFinder = findViewById(R.id.viewFinder);

            // Build the use case that provides a camera preview stream for displaying on-screen
            Preview preview = new Preview.Builder().build();
            preview.setSurfaceProvider(viewFinder.getSurfaceProvider());

            // Select back camera as a default
            CameraSelector cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA;

            // Build the use case for taking camera pictures
            imageCapture = new ImageCapture.Builder()
                    .setTargetRotation(viewFinder.getDisplay().getRotation())
                    .build();

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll();

                // Bind use cases (preview and image capture) to camera
                cameraProvider.bindToLifecycle(
                        this, cameraSelector, preview, imageCapture);

            } catch(Exception e) {
                Log.e(TAG, "Use case binding failed: ", e);
            }

        }, ContextCompat.getMainExecutor(this));
    }

    // Takes a photo and "stores it" in a in-memory buffer
    private void takePhoto() {
        // If the image capture use case is null, exit out of the method.
        // This will be null If you tap the photo button before image capture is set up.
        // Without the return statement, the app would crash if it was null.
        if (imageCapture == null) {
            return;
        }

        // Set up image capture listener, which is triggered after photo has been taken
        imageCapture.takePicture(
            ContextCompat.getMainExecutor(this), new ImageCapture.OnImageCapturedCallback() {

                @Override
                public void onCaptureSuccess(@NonNull ImageProxy image) {
                    // Do the upload in an AsyncTask, provide the image to upload
                    imageUploadAsyncTask = new ImageUploadAsyncTask();
                    imageUploadAsyncTask.execute(image);
                }

                @Override
                public void onError(@NonNull ImageCaptureException e) {
                    // TODO: Show an error message to the user
                    Log.e(TAG, "Photo capture failed: ", e);
                    finish(); // finish the activity
                }
            }
         );
    }

    // A dialog fragment for showing an alert dialog to the user, explaining why permission
    // to the camera is needed
    public static class CameraPermissionNeededDialogFragment extends DialogFragment {
        public static final String TAG = "CameraPermissionNeededDialogFragment";
        private final ActivityResultLauncher<String> requestPermissionLauncher;

        public CameraPermissionNeededDialogFragment(ActivityResultLauncher<String> requestPermissionLauncher) {
            this.requestPermissionLauncher = requestPermissionLauncher;
        }

        @NonNull
        @Override
        public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
            return new AlertDialog.Builder(requireContext())
                    .setTitle(R.string.dialog_title)
                    .setMessage(getString(R.string.camera_permission_needed))
                    .setIcon(R.drawable.ic_camera_capture)
                    .setPositiveButton(getString(R.string.ok), (dialog, which) -> {
                        // launch request of permission
                        requestPermissionLauncher.launch(Manifest.permission.CAMERA);
                    })
                    .setNegativeButton(getString(R.string.cancel), (dialog, which) -> requireActivity().finish())
                    .create();
        }
    }

    private class ImageUploadAsyncTask extends AsyncTask<ImageProxy, Integer, String> {
        private ViewGroup progressBarViewGroup;
        private ProgressBar progressBar;

        @Override
        protected void onPreExecute() {
            // Disable camera capture button
            findViewById(R.id.camera_capture_button).setEnabled(false);

            // Get references to the progressbar and its parent
            progressBar = findViewById(R.id.camera_upload_progress);
            progressBarViewGroup = findViewById(R.id.camera_upload);

            // Show the progress bar layout
            animateVisibility(progressBarViewGroup, View.VISIBLE);
        }

        @Override
        protected String doInBackground(ImageProxy... imageProxies) {
            // For easy access to the image proxy
            ImageProxy image = imageProxies[imageProxies.length - 1];

            URL url;
            HttpsURLConnection connection;
            try {
                // Create an URL and get a connection from it
                url = new URL(MainActivity.URL_GALLERY_ADD);
                connection = (HttpsURLConnection) url.openConnection();

                // Set various settings for the connection to do a post to the add.php page
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setUseCaches(false);
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

                // Get image as an array of bytes (crop and rotate it before)
                byte[] imageBytes = cropAndRotate(image);

                // Use current time as name of image
                String imageName = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS", Locale.US)
                        .format(System.currentTimeMillis()) + ".jpg";

                // Post image name and data as url encoded parameters
                StringBuilder parameters = new StringBuilder("imageName=");
                parameters.append(URLEncoder.encode(imageName,"UTF-8"));
                parameters.append("&imageData=");
                parameters.append( // first encode as base64 and then also url encode
                        URLEncoder.encode(
                                Base64.encodeToString(imageBytes, Base64.DEFAULT),StandardCharsets.UTF_8.name()));

               connection.setRequestProperty("Content-Length", Integer.toString(parameters.toString().length()));

                // Set max length of the progress bar to parameter size
                progressBar.setMax(parameters.toString().length());

                // Write image to server using this stream
                DataOutputStream out= new DataOutputStream(connection.getOutputStream());

                // Normally we would do this (write the entire string at once)...
                //out.writeBytes(parameters.toString());

                // ... but we want to publish progress of the upload so we upload chunks of data
                ByteArrayInputStream imageStream = new ByteArrayInputStream (parameters.toString().getBytes(StandardCharsets.UTF_8));
                byte[] buffer = new byte[1024];
                int bytesRead, totalBytesRead = 0;

                // Continue as long we are not cancelled and there are more data to upload
                while (!isCancelled() && ((bytesRead = imageStream.read(buffer)) > 0)) {
                    out.write(buffer, 0, bytesRead);
                    totalBytesRead += bytesRead;
                    publishProgress(totalBytesRead); // onProgressUpdate
                    /*try {
                        Thread.sleep(3); // simulate slow connection
                    } catch (InterruptedException e) {
                        break;
                    }*/
                }

                out.flush(); // write any buffered output left in the stream

                // Read response from server (if we get a single "OK" everything is ok)
                // But only if we did not get cancelled
                String response;
                if (!isCancelled()) {
                    Scanner in = new Scanner(connection.getInputStream());
                    response = in.nextLine();
                    in.close();
                }
                else {
                    response = getString(R.string.upload_cancelled); // if upload was canceled
                }

                // Close all resources we have used
                out.close();
                imageStream.close();
                image.close();
                connection.disconnect();

                // Return response from server (to onPostExecute)
                return response;
            } catch (IOException e) {
                Log.e(TAG, "Error adding image: " + e.getMessage());
                return e.getMessage();
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            // Only publish the latest value
            progressBar.setProgress(values[values.length - 1]);
        }

        @Override
        protected void onPostExecute(String result) {
            // Hide the progress layout
            animateVisibility(progressBarViewGroup, View.GONE);

            // Enable camera capture button
            findViewById(R.id.camera_capture_button).setEnabled(true);

            // Check response/result from upload
            if (result.equalsIgnoreCase("ok")) {
                Toast.makeText(getApplicationContext(), R.string.add_image_ok, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(), result, Toast.LENGTH_LONG).show();
            }

            finish(); // return to gallery (MainActivity)
        }

        // Animates a View sliding up/down from its parent view group
        private void animateVisibility(View view, int visibility) {
            ViewGroup parent = (ViewGroup) view.getParent();

            Transition transition = new Slide(Gravity.BOTTOM);
            transition.setDuration(600);
            transition.addTarget(view);

            TransitionManager.beginDelayedTransition(parent, transition);
            view.setVisibility(visibility);
        }

        // When dealing with a ImageProxy we need to crop and rotate the image
        // because the in-memory image buffer is without rotation applied
        private byte[] cropAndRotate(ImageProxy image) {
            // 1 - Convert image to Bitmap
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

            // 2 - Crop the Bitmap
            bitmap = Bitmap.createBitmap(bitmap,
                    image.getCropRect().left,
                    image.getCropRect().top,
                    image.getCropRect().width(),
                    image.getCropRect().height());

            // 3 - Rotate the Bitmap
            if(image.getImageInfo().getRotationDegrees() != 0) {
                Matrix rotationMatrix = new Matrix();
                rotationMatrix.postRotate(image.getImageInfo().getRotationDegrees());
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), rotationMatrix, true);
            }

            // 4 - Back to byte[]
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);

            return stream.toByteArray();
        }
    }
}