package se.miun.roberi.dt031g.gallery;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    /**
     * The URL to the start page of the Gallery web app.
     */
    public static final String URL_GALLERY = "https://dt031g.programvaruteknik.nu/gallery/";

    /**
     * The URL to the add image page of the Gallery web app.
     */
    public static final String URL_GALLERY_ADD = "https://dt031g.programvaruteknik.nu/gallery/add.php";

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get a reference to the WebView and load the gallery web page
        webView = findViewById(R.id.web_view);

        // Our WebView should handle URL loading instead of the default handler of URL's
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.equalsIgnoreCase(URL_GALLERY_ADD)) {
                    startActivity(new Intent(getApplicationContext(), CameraActivity.class));
                    return true; // stops current WebView from loading the URL
                }

                return false; // let current WebView continue loading the URL
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Load gallery (perhaps user returned from CameraActivity without taking a photo)
        webView.loadUrl(URL_GALLERY);
    }
}